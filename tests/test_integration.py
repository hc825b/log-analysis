import unittest
from networkx.drawing.nx_pydot import write_dot

from log_analysis import crawljax_model
from log_analysis.query_es_server import get_api_call_messages

from selenium_replay import replay

class TestIntegration(unittest.TestCase):
    def test_query_and_parse(self):
        log_parser = crawljax_model.CrawlJaxLogParser()
        log_parser.parse_message_sequence( get_api_call_messages() )

        count = 0
        for session in log_parser.iter_parsed_crawljaxsessions():
            count = count + 1
            target = crawljax_model.UIStateGraph()
            target.annotate_graph( session.iter_edges() )
            # TODO Add assertions

"""
    def test_replay_path_in_session(self):
        log_parser = crawljax_model.CrawlJaxLogParser()
        log_parser.parse_message_sequence( get_api_call_messages() )

        base_url = 'http://127.0.0.1:9080'
        counter = 1
        for session in log_parser.iter_parsed_crawljaxsessions():
            for path in session.iter_paths():
                name = "testcase-" + str(counter)
                counter = counter + 1
                replay.replay_path(name, base_url, path)
                if counter > 5:
                    return

    def test_replay_shortest_path(self):
        log_parser = crawljax_model.CrawlJaxLogParser()
        log_parser.parse_message_sequence( get_api_call_messages() )

        ui_graph = crawljax_model.UIStateGraph()
        for session in log_parser.iter_parsed_crawljaxsessions():
            ui_graph.annotate_graph( session.iter_edges() )

        s_path = ui_graph.shortest_path_to_edge 
"""
        

if ( __name__  == '__main__' ):
    unittest.main()
