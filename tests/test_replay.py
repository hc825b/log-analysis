import unittest

from selenium_replay import replay


class TestReplay(unittest.TestCase):
    def test_replay(self):
        cmd_list = [
            ('open', '/', ''),
            ('click', 'xpath=/HTML[1]/BODY[1]/A[1]', ''),
            ('click', 'xpath=/HTML[1]/BODY[1]/A[2]', ''),
        ]

        replay.replay('helloworld', 'http://127.0.0.1:50001', cmd_list)

    def test_replay_path(self):
        path = [
            {
                'src': None,
                'microservices': [('web', 'GET', '/')],
                'tgt': 'index',
                'eventable': None
            },
            {
                'src': 'index',
                'microservices': [('web', 'GET', '/')],
                'tgt': 'index',
                'eventable': {
                    'eventType': 'click',
                    'identification': 'xpath /HTML[1]/BODY[1]/A[1]'
                }
            },
            {
                'src': 'index',
                'microservices': [('web', 'GET', '/about/')],
                'tgt': 'state3',
                'eventable': {
                    'eventType': 'click',
                    'identification': 'xpath /HTML[1]/BODY[1]/A[2]'
                }
            }
        ]
        replay.replay_path('helloworld', 'http://127.0.0.1:50001', path)
