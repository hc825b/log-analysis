import unittest

from selenium_replay import selenese_html

class TestSeleneseHTML(unittest.TestCase):
    def test_translate_eventable(self):
        eventable_lst = [
            {'eventType': 'click', 'identification': 'xpath /HTML[1]/BODY[1]/A[1]'},
            {'eventType': 'click', 'identification': 'xpath /HTML[1]/BODY[1]/A[2]'}
        ]

        for eventable in eventable_lst:
            print( selenese_html.translate_eventable(eventable) )

    def test_render_selenese_html_testcase(self):
        eventable_lst = [
            {'eventType': 'click', 'identification': 'xpath /HTML[1]/BODY[1]/A[1]'},
            {'eventType': 'click', 'identification': 'xpath /HTML[1]/BODY[1]/A[2]'}
        ]
        target = selenese_html.SeleneseHtmlTestcase('helloworld')

        for eventable in eventable_lst:
            target.append( selenese_html.translate_eventable(eventable) )

        html = target.render_html()
        # TODO Add assertions
        print (html)


    def test_render_selenese_html_testsuite(self):
        # TODO
        pass
