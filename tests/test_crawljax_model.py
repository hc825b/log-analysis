import unittest
from networkx import to_edgelist
from networkx.drawing.nx_pydot import write_dot

from log_analysis import crawljax_model


class TestCrawlJaxModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.msg_lst = [
            {'message_type': 'LoadUrl'},
            {
                'message_type': 'ServerSideLog',
                'tag': 'web',
                'verb': 'GET',
                'request': '/rest/api/config/activeDataService'
            },
            {
                'message_type': 'ServerSideLog',
                'tag': 'web',
                'verb': 'GET',
                'request': '/rest/api/config/dataServices'
            },
            {
                'message_type': 'EventFired',
                'crawljax' : { 'eventable' : 'EVENTABLE' }
            },
            {
                'message_type': 'ChangedState',
                'crawljax' : {
                    'source': 'index',
                    'target': 'state1'
                }
            },
            {
                'message_type': 'EventFired',
                'crawljax' : { 'eventable' : 'EVENTABLE' }
            },
            {
                'message_type': 'ChangedState',
                'crawljax' : {
                    'source': 'state1',
                    'target': 'index'
                }
            },
            {
                'message_type': 'EventFired',
                'crawljax' : { 'eventable' : 'EVENTABLE' }
            },
            {
                'message_type': 'ChangedState',
                'crawljax' : {
                    'source': 'index',
                    'target': 'state3'
                }
            },
            {'message_type': 'LoadUrl'},
            {'message_type': 'Backtracking'},
            {
                'message_type': 'ChangedState',
                'crawljax' : {
                    'source': 'index',
                    'target': 'state1'
                }
            },
            {
                'message_type': 'ServerSideLog',
                'tag': 'web',
                'verb': 'GET',
                'request': '/rest/api/config/activeDataService'
            },
            {
                'message_type': 'ServerSideLog',
                'tag': 'web',
                'verb': 'GET',
                'request': '/rest/api/config/dataServices'
            },
            {
                'message_type': 'EventFired',
                'crawljax' : { 'eventable' : 'EVENTABLE' }
            }
        ]

        cls.edge_list = [
            {
                 'src': None, 'tgt': 'index',
                 'eventable': None,
                 'microservices': [('web', 'GET', '/')]
            },
            {
                 'src': 'index', 'tgt': 'index',
                 'eventable': {
                     'eventType': 'click',
                     'identification': 'xpath /HTML[1]/BODY[1]/A[1]'
                 },
                 'microservices': [('web', 'GET', '/')]
            },
            {
                 'src': None, 'tgt': 'index',
                 'eventable': None,
                 'microservices': [('web', 'GET', '/')]
            }
        ]

    def test_send(self):
        target = crawljax_model.CrawlJaxEdgeCollector('default')

        for msg in self.msg_lst:
            target.send( crawljax_model.AutomatonInput(msg) )
        # TODO Add assertion

    def test_reset(self):
        # TODO
        pass

    def test_parse_message_sequence(self):
        target = crawljax_model.CrawlJaxLogParser()
        target.parse_message_sequence( iter(self.msg_lst) )
        #TODO Add assertion


    def test_session(self):
        target = crawljax_model.CrawlJaxSession(self.edge_list)

        for path in target.iter_paths():
            # TODO Add assertions
            print(path)

    def test_annotate_graph(self):
        target = crawljax_model.UIStateGraph()

        target.annotate_graph(self.edge_list)
        #TODO Add assertions
        write_dot(target, 'ui_graph.gv')

    def test_shortest_path_to_edge(self):
        target = crawljax_model.UIStateGraph()
        target.annotate_graph(self.edge_list)

        edge = {
                 'src': 'index', 'tgt': 'index',
                 'eventable': {
                     'eventType': 'click',
                     'identification': 'xpath /HTML[1]/BODY[1]/A[1]'
                 },
                 'microservices': [('web', 'GET', '/')]
        }

        #TODO Add assertions
        print( target.shortest_path_to_edge( edge ) )

if ( __name__  == '__main__' ):
    unittest.main()
