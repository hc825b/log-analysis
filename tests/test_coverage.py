import unittest

from log_analysis import coverage

# TODO Change to parametrized tests

class TestAPICallGraph(unittest.TestCase):
    def test_match_node_01(self):
        # TODO
        pass

class TestComputeCoverage(unittest.TestCase):

    def setUp(self):
        self.api_call_graph = self.__acmeair_api_call_graph()

    def test_update_covered_edges_01(self):
        msg = {
            'tag'     : 'auth',
            'verb'    : 'POST',
            'request' : '/acmeair-auth-service/rest/api/authtoken/byuserid/uid0@email.com'
        }

        target = coverage.EdgeCoverage(self.api_call_graph)
        target.update_covered_edges( msg )

        src = ("web","POST","/rest/api/login")
        dst = ("auth","POST","/acmeair-auth-service/rest/api/authtoken/byuserid/")

        # TODO Add assertions
        #self.assertEqual( target.__graph[src][dst].get('covered'), 'must' )

    def test_compute_coverage_01(self):
        target = coverage.EdgeCoverage(self.api_call_graph)
        # TODO
        target.compute_coverage()

    def test_statistics(self):
        print ( "#Nodes:", len(self.api_call_graph), "#Edges:", self.api_call_graph.size() )

    @staticmethod
    def __acmeair_api_call_graph():
        G = coverage.APICallGraph()

        G.add_edge("clients", ("web","POST","/rest/api/login"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/login/logout"), covered=None)
        G.add_edge("clients", ("web","POST","/rest/api/flights/queryflights"), covered=None)
        G.add_edge("clients", ("web","POST","/rest/api/bookings/bookflights"), covered=None)
        G.add_edge("clients", ("web","POST","/rest/api/bookings/cancelbooking"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/bookings/byuser/"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/customer/byid/"), covered=None)
        G.add_edge("clients", ("web","POST","/rest/api/customer/byid/"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/runtime"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/dataServices"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/activeDataService"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countBookings"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countCustomers"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countSessions"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countFlights"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countFlightSegments"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/config/countAirports"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/loader/load"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/loader/query"), covered=None)
        G.add_edge("clients", ("web","GET", "/rest/api/checkstatus"), covered=None)
        #G.add_edge("clients", ("web", "GET","/rest/api/hystrix.stream"), covered=None)
 
        G.add_edge(("web","POST","/rest/api/login"),
                   ("auth","POST","/acmeair-auth-service/rest/api/authtoken/byuserid/"), covered=None)
        G.add_edge(("web","GET","/rest/api/login/logout"),
                   ("auth","DELETE","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","POST","/rest/api/flights/queryflights"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","POST","/rest/api/bookings/bookflights"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","POST","/rest/api/bookings/cancelbooking"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","GET", "/rest/api/bookings/byuser/"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","GET", "/rest/api/customer/byid/"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge(("web","POST", "/rest/api/customer/byid/"),
                   ("auth","GET","/acmeair-auth-service/rest/api/authtoken/"), covered=None)
        G.add_edge("web",
                   ("auth","GET","/acmeair-auth-service/rest/api/status"), covered=None)
        return G

if ( __name__  == '__main__' ):
    unittest.main()
