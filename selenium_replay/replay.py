from selenese_html import SeleneseHtmlTestcase, translate_event

def replay_path(name, base_url, path):
    cmd_list = list()

    for edge in path:
        cmd_list.extend( translate_event( edge ) )

    replay(name, base_url, cmd_list)


def replay(name, base_url, cmd_list):
    testcase = SeleneseHtmlTestcase(name, base_url)

    testcase.cmd_list.extend(cmd_list)

    with open( name + '.html', 'w') as out_file:
        out_file.write( testcase.render_html() )
