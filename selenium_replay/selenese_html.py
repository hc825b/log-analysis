"""
This module provides APIs to convert given explored paths to executable
testcases for Selenium-IDE.

The output format follows Selenese specification described at
`Selenium-IDE
<http://docs.seleniumhq.org/docs/02_selenium_ide.jsp#script-syntax>`_
and
`Selenium Core
<http://release.seleniumhq.org/selenium-core/1.0.1/reference.html>`_.
"""

from collections import namedtuple
from jinja2 import Environment, PackageLoader

def check_command(command, target, value):
    assert isinstance(command, basestring)
    assert isinstance(target,  basestring)
    assert isinstance(value,   basestring)
    # TODO
    return True

def translate_event(crawljax_event):
    if crawljax_event['src'] == None:
        assert crawljax_event['tgt'] == 'index'
        assert crawljax_event['eventable'] == None
        # Load URL
        cmds = [translate_LoadUrl()]
    else:
        # TODO Support filling forms
        cmds = [translate_eventable(crawljax_event['eventable'])]
    return cmds

def translate_LoadUrl():
    return ('open', '/', '')

def translate_eventable(eventable):
    ident = eventable['identification']
    element_locator = _translate_locator(ident)

    try:
        ret = {
            'click' : ('click', element_locator, ''),
        }[ eventable['eventType'] ]

        # TODO Check if the command takes correct type of arguments
        #command, target, value = ret
        return ret
    except KeyError:
        raise UserWarning('Unsupported event type: ', eventable['eventType'])

def _translate_locator(locator):
    temp = locator.split(None, 1)
    loc_type = temp[0]
    loc_expr = temp[1]

    try:
        ret = {
            'xpath': 'xpath='+loc_expr
        }[ loc_type ]

        return ret
    except KeyError:
        raise UserWarning('Unsupported element locator type: ', loc_type)


class SeleneseHtmlTestcase():
    def __init__(self, name, base_url):
        """ Construct a testcase """
        self.__name = name
        self.__url = base_url
        self.cmd_list = list()

    def append(self, cmd):
        """  """
        # TODO check_command()
        self.cmd_list.append(cmd)

    def render_html(self):
        """ Render testcase """
        loader = PackageLoader('selenium_replay', package_path='templates')
        env = Environment(loader=loader)
        template = env.get_template('selenese_html_testcase.html')

        output = template.render(
            testcase_name=self.__name,
            base_url = self.__url,
            command_list=self.cmd_list
        )
        return output


class SeleneseHtmlTestsuite():
    def __init__(self, name):
        # TODO
        """ Construct a testsuite """
        raise NotImplementedError
