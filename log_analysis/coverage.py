from networkx import DiGraph
from networkx import to_edgelist, freeze

class APICallGraph(DiGraph):
    def __str__(self):
        return str( to_edgelist( self ) )

    # TODO validate graph structure

    def match_edges(self, msg):
        tag  = msg['tag']
        verb = msg['verb']
        req  = msg['request']
        def match_msg_with_api(n):
            # TODO Different APIs may share same prefix
            if tag == n[0] and verb == n[1] and req.startswith(n[2]):
                return True
            return False
        # XXX Better way to search matched node instead of searching all nodes in the graph
        nodes = filter(match_msg_with_api, self.nodes())
        if len( nodes ) == 0:
            raise UserWarning("HTTP request does not match any API ", (tag, verb, req))
        elif len( nodes ) >= 2:
            # TODO print all matched APIs
            raise UserWarning("HTTP request matches " + str(len(nodes)) +  " APIs ", (tag, verb, req))

        # TODO some message may contain agent and can match edges directly
        matched_node = nodes[0]
        edges = self.in_edges(matched_node)
        if len(edges) == 0:
            raise UserWarning("Matched API has no caller ", (tag, ver, req))
        return edges


class EdgeCoverage:
    def __init__(self, api_graph):
        self.__graph = api_graph
        freeze(self.__graph)

    def update_covered_edges(self, api_call_msg):
        try:
            edges = self.__graph.match_edges(api_call_msg)
            assert( len(edges) != 0 )
            if len(edges) == 1:
                # The only incomming edge of the node must be covered
                is_covered = "must"
            else:
                is_covered = "may"

            for (src, dst) in edges:
                self.__graph[src][dst]['covered'] = is_covered
        except UserWarning as e:
            # TODO Handle Exception
            pass

    def compute_coverage(self):
        edges = self.__graph.edges(data=True)
        num_must = sum (e[2].get('covered') == 'must' for e in edges)
        num_may  = sum (e[2].get('covered') == 'may'  for e in edges)
        num_not  = sum (e[2].get('covered') == None   for e in edges)

        return {"must" : num_must, "may" : num_may, "not_covered" : num_not}
