from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q


def get_api_call_messages():

    client = Elasticsearch()

    s = Search(using=client, index="logstash-*") \
        .query(~Q("match", tags="_grokparsefailure")) \
        .sort('@timestamp', 'offset').params(preserve_order=True)

    return s.scan()
