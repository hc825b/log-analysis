from networkx import MultiDiGraph, shortest_path
from automaton import machines, runners, exceptions

from selenium_replay.selenese_html import translate_LoadUrl, translate_eventable

class AutomatonInput:
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)

    def __hash__(self):
        return hash( self.msg['message_type'] )

    def __eq__(self, other):
        return hash(self) == hash(other)

    def get_microservice_query(self):
        # TODO Extract only necessary information from msg
        # NOTE Return value should be hashable and immutable
        return (self.msg['tag'], self.msg['verb'], self.msg['request'],)

class CrawlJaxEdgeCollector:
    def __init__(self, thread):
        self.__thread = thread
        if thread == "main":
           fa = self.__build_finite_automaton_main()
        else:
           fa = self.__build_finite_automaton_task()
        fa.freeze() # Prevent changes on the automaton

        self.__runner = runners.FiniteRunner( fa )
        self.init()

    def init(self):
        self.labeled_edges = []
        self.reset()

    def reset(self):
        self.__iter = None
        self.__cur_ui_state = None
        self.__cur_label = CrawlJaxEdgeCollector.__blank_label()

    def send(self, automaton_input):
        if self.__iter:
            return self.__iter.send( automaton_input )

        self.__iter = self.__runner.run_iter( automaton_input )
        return self.__iter.send(None)

    @staticmethod
    def __blank_label():
        return {'eventable': None, 'microservices': list()}

    def __build_finite_automaton_main(self):
        """
        Client-side Messages of the main thread should be accepted by
        the automaton given below.
        """
        fa = machines.FiniteMachine('s0')

        fa.add_state('s0', on_enter=self.__aggregate_server_side_log)
        fa.add_state('s1', on_enter=self.__load_url)

        fa.add_transition('s0', 's0', 'ServerSideLog')
        fa.add_transition('s0', 's1', 'LoadUrl')

        # XXX Add more states and transitions to get more precise information
        return fa

    def __build_finite_automaton_task(self):
        """
        Client-side Messages of a task thread should be accepted by
        the automaton given below.
        """

        fa = machines.FiniteMachine('s0')
        # TODO Parse filled form data

        fa.add_state('s0', on_enter=self.__aggregate_server_side_log)
        fa.add_state('s1', on_enter=self.__enter_s1)
        fa.add_state('s2', on_enter=self.__enter_s2)
        fa.add_state('s3', on_enter=self.__event_fired)
        fa.add_state('s4', on_enter=self.__backtracking)
        fa.add_state('s5', on_enter=self.__aggregate_server_side_log)
        fa.add_state('s6', on_enter=self.__enter_s6)

        fa.add_transition('s0', 's0', 'ServerSideLog')
        fa.add_transition('s0', 's1', 'LoadUrl')
        fa.add_transition('s1', 's1', 'LoadUrl')
        fa.add_transition('s1', 's2', 'ServerSideLog')
        fa.add_transition('s1', 's3', 'EventFired')
        fa.add_transition('s1', 's4', 'Backtracking')
        fa.add_transition('s2', 's1', 'LoadUrl')
        fa.add_transition('s2', 's2', 'ServerSideLog')
        fa.add_transition('s2', 's3', 'EventFired')
        fa.add_transition('s3', 's1', 'LoadUrl')
        fa.add_transition('s3', 's2', 'ChangedState')
        fa.add_transition('s3', 's3', 'EventFired')
        fa.add_transition('s3', 's5', 'ServerSideLog')
        fa.add_transition('s4', 's6', 'ChangedState')
        fa.add_transition('s5', 's3', 'EventFired')
        fa.add_transition('s5', 's5', 'ServerSideLog')
        fa.add_transition('s6', 's1', 'EventFired')
        fa.add_transition('s6', 's6', 'ServerSideLog')
        return fa

    def __enter_s1(self, state, transition):
        if transition == 'LoadUrl':
           self.__load_url(state, transition)
        elif transition == 'EventFired':
           self.__event_fired_backtracking(state, transition)

    def __enter_s2(self, state, transition):
        if transition == 'ServerSideLog':
           self.__aggregate_server_side_log(state, transition)
        elif transition == 'ChangedState':
           self.__changed_state(state,transition)

    def __enter_s6(self, state, transition):
        if transition == 'ServerSideLog':
           self.__aggregate_server_side_log_backtracking(state, transition)
        elif transition == 'ChangedState':
           self.__changed_state_backtracking(state,transition)

    def __backtracking(self, state, transition):
        pass # TODO Add logging messages

    def __aggregate_server_side_log(self, state, transition):
        microservice_query = transition.get_microservice_query()
        self.__cur_label['microservices'].append( microservice_query )

    def __aggregate_server_side_log_backtracking(self, state, transition):
        microservice_query = transition.get_microservice_query()
        self.labeled_edges[-1]['microservices'].append( microservice_query )

    def __load_url(self, state, transition):
        self.__cur_ui_state = None
        self.__collect_edge('index')
        self.__cur_ui_state = 'index'

    def __event_fired(self, state, transition):
        eventable = transition.msg['crawljax']['eventable']
        self.__cur_label['eventable'] = eventable
        self.__collect_edge(self.__cur_ui_state) # Create self-loop edge

    def __event_fired_backtracking(self, state, transition):
        eventable = transition.msg['crawljax']['eventable']
        self.labeled_edges[-1]['eventable'] = eventable

    def __changed_state(self, state, transition):
        assert self.__cur_ui_state == transition.msg['crawljax']['source']
        tgt = transition.msg['crawljax']['target']
        self.labeled_edges[-1]['tgt'] = tgt
        self.__cur_ui_state = tgt

    def __changed_state_backtracking(self, state, transition):
        assert self.__cur_ui_state == transition.msg['crawljax']['source']
        tgt = transition.msg['crawljax']['target']
        self.__collect_edge(tgt)
        self.__cur_ui_state = tgt

    def __collect_edge(self, tgt):
        assert tgt != None
        src = self.__cur_ui_state 

        self.__cur_label.update({'src': src, 'tgt': tgt})
        self.labeled_edges.append( self.__cur_label )

        # Reset label to parse next edge
        self.__cur_label = CrawlJaxEdgeCollector.__blank_label()


class CrawlJaxLogParser:
    def __init__(self):
        self.__fa_dict = dict()
        self.__fa_dict['default'] = CrawlJaxEdgeCollector('default')
        self.__parsed_sessions = list()

    def parse_message_sequence(self, msg_iter):

        # Derive a list of labels on edges from log messages
        state_iter = self.__fa_dict['default'] # XXX Only support 1 user
        for msg in msg_iter:
            if msg['message_type'] == 'FillForm':
                # TODO Handle Form fields.
                # Should be implemented in CrawlJaxEdgeCollector
                continue
            elif msg['message_type'] == 'PreCrawling':
                # TODO Start parsing a new crawling session
                state_iter = self.__fa_dict['default']
                state_iter.init()
                continue
            elif msg['message_type'] == 'PostCrawling':
                # TODO Finishing parsing current session.
                self.__parsed_sessions.append(
                    CrawlJaxSession(state_iter.labeled_edges)
                )
                state_iter = None
                continue
            # XXX temporary solution to handle main thread messages
            elif 'thread' in msg and msg['thread'] == 'main':
                continue
            try:
                if state_iter == None:
                    raise UserWarning("Skip messages before/after crawling",
                      (msg['@timestamp'], msg['message_type'], msg['message']))
                # TODO state_iter = self.__fa_dict[ msg['thread'] ]
                state_iter.send( AutomatonInput(msg) )
            except exceptions.NotFound:
                # TODO Turn into log
                print("Skip unexpected log message",
                      (msg['@timestamp'], msg['message_type'], msg['message']))
                state_iter.reset()
                # Pop the last collected edge since the unexpected log message
                # can be due to interrupt. The last event might not finish.
                if len(state_iter.labeled_edges) != 0:
                    state_iter.labeled_edges.pop()

                if msg['message_type'] == 'LoadUrl':
                    state_iter.send( AutomatonInput(msg) )
            except UserWarning as e:
                print(e)

    def iter_parsed_crawljaxsessions(self):
        for session in self.__parsed_sessions:
            yield session


class CrawlJaxSession:
    """
    A session is composed of multiple paths starting from 'index' node of the
    constructed UIStateGraph.
    """
    def __init__(self, crawljax_edges):
        self.__path_list = list()
        self.__split(crawljax_edges)

    def iter_paths(self):
        for path in self.__path_list:
            yield path

    def iter_edges(self):
        for path in self.__path_list:
            for edge in path:
                yield edge

    def __split(self, labeled_edges):
        for edge in labeled_edges:
            if edge['src'] == None: # Special edges represent LoadURL
                self.__path_list.append( list() )
            self.__path_list[-1].append( edge )


class UIStateGraph(MultiDiGraph):
    def annotate_graph(self, edge_list):
        # Reconstruct and annotate graph with the list of edges
        for e in edge_list:
            self.add_edge_unique_label(str(e['src']), str(e['tgt']), label=e)

    def shortest_path_to_edge(self, edge):
        """
        Compute a list of edges starting from 'index' node to given edge (including the given edge)
        """
        if edge['src'] == None:
           # Turn into log message
           #print("The given edge doesn't have source node.")
           return edge

        node_list = shortest_path(self, source = 'index', target=edge['src'])
        src_list = ([str(None)] + node_list)
        src_list.pop()
        tgt_list = node_list

        # NOTE Can choose othe parallel edges with key!=0
        edge_list = [ self[src][tgt][0]['label'] for src, tgt in zip(src_list, tgt_list)]

        edge_list.append(edge)
        return edge_list

    def add_edge_unique_label(self, src, tgt, label):
        # Remove duplicate labels
        # i.e. labels with identical eventable and microservices
        d = self.get_edge_data(src, tgt)
        if d != None:
            for value in d.itervalues():
                # NOTE Compare labels by string representation
                if str(value['label']) == str(label):
                    return
        self.add_edge(src, tgt, label=label)
